import React, {useState, useContext} from 'react';
import {Context as FavContext} from '../context/favoriteContext';
import {View, Button, StyleSheet, Text, Image} from 'react-native';
import {TouchableWithoutFeedback, FlatList} from 'react-native-gesture-handler';
import {useNavigation} from '@react-navigation/native';

const Favorite = () => {
  const {state} = useContext(FavContext);
  const navigation = useNavigation();
  return (
    <View>
      <FlatList
        data={state}
        keyExtractor={item => item.id.toString()}
        renderItem={({item}) => {
          return (
            <View style={styles.container}>
              <View style={styles.row}>
                <TouchableWithoutFeedback
                  onPress={() => {
                    navigation.navigate('DetailScene', {data: item});
                  }}>
                  <Image
                    style={styles.imgPoster}
                    source={{
                      uri: `https://image.tmdb.org/t/p/w92${item.poster_path}`,
                    }}
                  />
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback
                  onPress={() => {
                    navigation.navigate('DetailScene', {data: item});
                  }}>
                  <Text style={styles.titleText}>{item.title}</Text>
                  <Text style={styles.dateText}>{item.release_date}</Text>
                  <Text numberOfLines={4} style={styles.overviewText}>
                    {item.overview}
                  </Text>
                </TouchableWithoutFeedback>
              </View>
            </View>
          );
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    margin: 5,
  },
  row: {
    flexDirection: 'row',
    alignContent: 'center',
  },
  column: {
    flexDirection: 'column',
    alignContent: 'center',
    margin: 5,
  },
  defaultText: {
    fontSize: 16,
    width: 300,
  },
  titleText: {
    fontWeight: 'bold',
    fontSize: 16,
  },
  dateText: {
    color: 'grey',
  },
  overviewText: {
    width: 300,
    marginTop: 3,
    marginRight: 10,
  },
  imgPoster: {
    width: 100,
    height: 150,
    margin: 5,
  },
});
export default Favorite;
