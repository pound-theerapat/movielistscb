import createDataContext from './createContext';

//Reducer
const favoriteReducer = (state, {type, payload}) => {
  switch (type) {
    case 'add_favorite':
      return [...state, payload];
    case 'remove_favorite':
      return state.filter(item => item !== payload);
    default:
      return state;
  }
};

//action
const addFavorite = dispatch => {
  return item => {
    dispatch({type: 'add_favorite', payload: item});
  };
};
const removeFavorite = dispatch => {
  return item => {
    dispatch({type: 'remove_favorite', payload: item});
  };
};

export const {Context, Provider} = createDataContext(
  favoriteReducer,
  {addFavorite, removeFavorite},
  [],
);
